/*
 * Copyright (c) 2018 Lars Thrane A/S
 * SPDX-License-Identifier: GPL-2.0
 *
 * Telnet interface
 */
#ifndef TELNET_H
#define TELNET_H

typedef int     (srv_tx_f) (void *cctx, const void *buf, int len);
typedef int     (srv_rx_f) (void *cctx, int timeout);
typedef void    (srv_ms_f) (void *cctx, int status);

/* Telnet context */
typedef struct {
    void           *cctx;       /* Client context */

    srv_tx_f       *srv_tx;     /* Write to server */
    srv_rx_f       *srv_rx;     /* Read from server */
    srv_ms_f       *srv_ms;     /* Modem status from server */

    unsigned int    bytes_rx;   /* Rx byte count */
    unsigned int    off_rx;     /* Rx buffer offset */
    unsigned char   mode_telnet;        /* Telnet mode */
    unsigned char   mode_rfc2217;       /* RFC-2217 mode */
    unsigned char   telnet_ack[256];    /* telnet option ack counter */
} tn_ctx_t;

extern tn_ctx_t *telnet_ctx_init(void *cctx, srv_tx_f * ftx, srv_rx_f * frx,
                                 srv_ms_f * fss);

extern int      telnet_open(tn_ctx_t * tcc);

extern int      telnet_rx(tn_ctx_t * tcc, char *buf, int *plen);
extern int      telnet_tx(tn_ctx_t * tcc, const char *buf, int len);

extern void     telnet_set_opt(tn_ctx_t * tcc, int opt, int prm,
                               const void *val, int len);

extern void     telnet_rfc2217_handle_opt(tn_ctx_t * tcc, int opt,
                                          unsigned char *pu, int nd);

extern void     telnet_rfc2217_cfg(tn_ctx_t * tcc, int cmd,
                                   const void *val, int nd);
extern void     telnet_rfc2217_ctl(tn_ctx_t * tcc, unsigned int val);

/*RFC2217 modem state*/
#define TNS_STATE_DCD    0x80
#define TNS_STATE_RI     0x40
#define TNS_STATE_DSR    0x20
#define TNS_STATE_CTS    0x10

/*RFC2217 configuration commands*/
#define TNS_SET_BAUDRATE    1
#define TNS_SET_DATASIZE    2
#define TNS_SET_PARITY      3
#define TNS_SET_STOPSIZE    4
#define TNS_SET_CONTROL     5
#define TNS_SET_PURGE      12

/*RFC2217 control commands*/
#define TNS_CTL_NOFLOW      1
#define TNS_CTL_XONOFF_OUT  2
#define TNS_CTL_CTSRTS      3
#define TNS_CTL_DTR_ON      8
#define TNS_CTL_DTR_OFF     9
#define TNS_CTL_RTS_ON     11
#define TNS_CTL_RTS_OFF    12
#define TNS_CTL_XONOFF_IN  15

#endif /*TELNET_H */
